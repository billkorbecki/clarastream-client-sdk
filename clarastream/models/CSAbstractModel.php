<?php
/**
 * Created by PhpStorm.
 * User: bthibault
 * Date: 5/16/14
 * Time: 12:53 PM
 */

abstract class CSAbstractModel {

    protected $_rawData = null;

    public function __construct($rawData = array())
    {
        $this->_rawData = $rawData;
    }

    public function getRawData()
    {
        // Copy the original
        $tempData = $this->_rawData;
        return $tempData;
    }

    public function getId()
    {
        return isset($this->_rawData['id']) ? $this->_rawData['id'] : null;
    }

} 