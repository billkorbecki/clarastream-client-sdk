<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/18/14
 * Time: 10:09 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');
require_once(dirname(__FILE__) . '/CSCategoryRef.php');

class CSCategory extends CSAbstractModel
{

    private $_ancestors = null;
    private $_children = null;

    public function getCategoryId()
    {
        return (isset($this->_rawData['categoryId']) ? $this->_rawData['categoryId'] : 0);
    }

    public function setCategoryId($categoryId)
    {
        $this->_rawData['categoryId'] = $categoryId;
    }

    public function getName()
    {
        return isset($this->_rawData['name']) ? $this->_rawData['name'] : null;
    }

    public function setName($name)
    {
        $this->_rawData['name'] = $name;
    }

    public function getParentCategory()
    {
        return isset($this->_rawData['parentCategory']) ? new CategoryRef($this->_rawData['parentCategory']) : null;
    }

    public function getAncestors()
    {
        if ($this->_ancestors == null)
        {
            $this->_ancestors = $this->createCategoryRefArray($this->_rawData, 'ancestors');
        }
        return $this->_ancestors;
    }

    public function getChildren()
    {
        if ($this->_children == null)
        {
            $this->_children = $this->createCategoryRefArray($this->_rawData, 'children');
        }
        return $this->_children;
    }

    private function createCategoryRefArray($data, $attributeId)
    {
        $categoryRefs = array();
        if (isset($data[$attributeId]) && is_array($data[$attributeId]))
        {
            foreach ($data[$attributeId] as $categoryRefData)
            {
                array_push($categoryRefs, new CSCategoryRef($categoryRefData));
            }
        }
        return $categoryRefs;
    }
}