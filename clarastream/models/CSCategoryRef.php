<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/18/14
 * Time: 10:09 AM
 */
require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSCategoryRef extends CSAbstractModel
{
    public function getCategoryId()
    {
        return (isset($this->_rawData['categoryId']) ? $this->_rawData['categoryId'] : 0);
    }

    public function getName()
    {
        return isset($this->_rawData['name']) ? $this->_rawData['name'] : null;
    }
}