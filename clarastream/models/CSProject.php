<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 11:24 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSProject extends CSAbstractModel
{

    public function getName()
    {
        return isset($this->_rawData['name']) ? $this->_rawData['name'] : null;
    }

    public function setName($name)
    {
        $this->_rawData['name'] = $name;
    }

    public function getUserId()
    {
        return isset($this->_rawData['userId']) ? $this->_rawData['userId'] : null;
    }

    public function getTemplateId()
    {
        return isset($this->_rawData['templateId']) ? $this->_rawData['templateId'] : null;
    }

    public function setTemplateId($templateId)
    {
        $this->_rawData['templateId'] = $templateId;
    }

    public function getTotalSKUCount()
    {
        return (isset($this->_rawData['totalSKUCount']) ? $this->_rawData['totalSKUCount'] : 0);
    }

    public function getTotalRecordCount()
    {
        return (isset($this->_rawData['totalRecordCount']) ? $this->_rawData['totalRecordCount'] : 0);
    }

    public function getProductCount()
    {
        return (isset($this->_rawData['productCount']) ? $this->_rawData['productCount'] : 0);
    }
}