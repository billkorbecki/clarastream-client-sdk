<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/24/14
 * Time: 12:28 AM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSSyncData extends CSAbstractModel
{
    public function getProjectId()
    {
        return isset($this->_rawData['projectId']) ? $this->_rawData['projectId'] : null;
    }

    public function getSyncProductIds()
    {
        return isset($this->_rawData['syncProductIds']) ? $this->_rawData['syncProductIds'] : null;
    }
}