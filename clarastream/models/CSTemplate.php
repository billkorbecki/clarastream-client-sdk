<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 12:59 PM
 */

require_once(dirname(__FILE__) . '/CSAbstractModel.php');

class CSTemplate extends CSAbstractModel
{

    public function getName()
    {
        return isset($this->_rawData['name']) ? $this->_rawData['name'] : null;
    }

    public function setName($name)
    {
        $this->_rawData['name'] = $name;
    }

    public function getUserId()
    {
        return isset($this->_rawData['userId']) ? $this->_rawData['userId'] : null;
    }

    public function getExtraOptions()
    {
        return isset($this->_rawData['extraOptions']) ? $this->_rawData['extraOptions'] : null;
    }

    public function setExtraOptions($extraOptions)
    {
        $this->_rawData['extraOptions'] = $extraOptions;
    }

    // TODO: (WK) Add "add/remove" methods for individual options
}