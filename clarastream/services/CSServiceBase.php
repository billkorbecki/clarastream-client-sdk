<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/24/14
 * Time: 9:51 AM
 */

require_once(dirname(__FILE__) . "/../../vendor/autoload.php");

use Guzzle\Http\Client;

abstract class CSServiceBase implements ICSService
{
    private $_baseServiceURL = "";
    private $_serviceClient = null;
    private $_serviceAuthDelegate = null;

    public function __construct($baseServiceURL, $serviceAuthDelegate)
    {
        $this->_baseServiceURL = $baseServiceURL;
        $this->_serviceClient = new Client($baseServiceURL);
        $this->_serviceAuthDelegate = $serviceAuthDelegate;
    }

    protected function getBaseServiceURL()
    {
        return $this->_baseServiceURL;
    }

    protected function processCallToURL($url, $queryParams = null, $dataArray = null, $requestType = "POST")
    {
        // TODO: (WK) Add protective code for invalid calls...

        $responseArray = null;
        $allowBody = false;

        try
        {
            $request = null;
            if ($requestType == "GET")
            {
                $request = $this->_serviceClient->get($url);
            }
            else if ($requestType == "PUT")
            {
                $request = $this->_serviceClient->put($url);
                $allowBody = true;
            }
            else
            {
                $request = $this->_serviceClient->post($url);
                $allowBody = true;
            }

            if ($queryParams != null)
            {
                $query = $request->getQuery();
                foreach ($queryParams as $key => $value)
                {
                    $query->set($key, $value);
                }
            }

            error_log("CSServiceBase::processCallToURL: [$requestType Request] requestURL = " . $request->getUrl());

            if ($this->_serviceAuthDelegate != null)
            {
                $this->_serviceAuthDelegate->configureGuzzleAuthentication($request);
            }

            if ($allowBody && $dataArray)
            {
                $data_string = json_encode($dataArray);

                error_log("CSServiceBase::processCallToURL: using data = $data_string");

                $request->setBody($data_string, 'application/json');
            }

            $response = $request->send();
            $responseArray = $response->json();

            $responseArrayString = print_r($responseArray, true);
            error_log("CSServiceBase::processCallToURL: response = $responseArrayString");
        }
        catch (Guzzle\Http\Exception\BadResponseException $e)
        {
            error_log(
                "CSServiceBase::processCallToURL: error calling url: " . $e->getRequest()->getUrl() .
                ": errStatusCode = " . $e->getResponse()->getStatusCode() .
                ": errMessage = " . $e->getMessage());

            // TODO: (WK) throw custom error here...
            throw $e;
        }
        catch (Exception $ex)
        {
            error_log("CSServiceBase::processCallToURL: error calling url: $url");
            // TODO: (WK) throw custom error here...
            throw $ex;
        }

        return $responseArray;
    }
} 