<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/2/14
 * Time: 2:40 PM
 */

interface ICSServiceAuthDelegate
{
    public function configureGuzzleAuthentication($guzzleRequest);
}