<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/ICSCategoryService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSCategory.php');

class CSCategoryService extends CSServiceBase implements ICSCategoryService
{
    public function getCategoriesForCart($cartId, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/category/cartId/' . $cartId;

        $queryParams = array();
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $categoryList = array();
        if (isset($responseArray['categories']))
        {
            foreach ($responseArray['categories'] as $categoryData)
            {
                if ($categoryData != null)
                {
                    array_push($categoryList, new CSCategory($categoryData));
                }
            }
        }


        return $categoryList;
    }

    public function getCategoriesByCategoryId($categoryId, $cartId = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/category/categoryId/' . $categoryId;

        $queryParams = array();
        if ($cartId != null)
        {
            $queryParams['cartId'] = "" . $cartId;
        }
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $categoryList = array();
        if (isset($responseArray['categories']))
        {
            foreach ($responseArray['categories'] as $categoryData)
            {
                if ($categoryData != null)
                {
                    array_push($categoryList, new CSCategory($categoryData));
                }
            }
        }


        return $categoryList;
    }

    public function getCategory($objectId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/category/' . $objectId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['category']) ? new CSCategory($responseArray['category']) : null;
    }

    public function createRootCategory($category, $cartId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/category/cartId/' . $cartId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["categoryData"] = ($category != null) ? $category->getRawData() : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['category']) ? new CSProduct($responseArray['category']) : null;
    }

    public function createChildCategory($category, $parentId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/category/' . $parentId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["categoryData"] = ($category != null) ? $category->getRawData() : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['category']) ? new CSProduct($responseArray['category']) : null;
    }

    public function deleteCategoriesForCart($cartId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/category/cartId/' . $cartId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "DELETE");

        // TODO (WK) Determine what to do with error situations...

        return;
    }
}