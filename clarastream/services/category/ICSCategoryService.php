<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSCategoryService extends ICSService
{
    public function getCategoriesForCart($cartId, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
    public function getCategoriesByCategoryId($categoryId, $cartId = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
    public function getCategory($objectId, $masterUserName = null, $masterUserId = null);
    public function createRootCategory($category, $cartId, $masterUserName = null, $masterUserId = null);
    public function createChildCategory($category, $parentId, $masterUserName = null, $masterUserId = null);
    public function deleteCategoriesForCart($cartId, $masterUserName = null, $masterUserId = null);

}