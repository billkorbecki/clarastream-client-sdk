<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/24/14
 * Time: 9:51 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSPluginService extends ICSService
{
    public function getVersions($type, $masterUserName = null, $masterUserId = null);
    public function getPlugin($id, $toFile, $masterUserName = null, $masterUserId = null);
}