<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSProductService extends ICSService
{
    public function getBaseProducts($type = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
    public function getProduct($productId, $masterUserName = null, $masterUserId = null);
    public function getBaseProductBySKU($productSKU, $masterUserName = null, $masterUserId = null);
    public function createBaseProduct($product, $mainImageURL = null, $subImageURLs = array(), $masterUserName = null, $masterUserId = null);
    public function getBaseProductVariations($offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
}