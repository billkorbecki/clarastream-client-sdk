<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 11:23 AM
 */

require_once(dirname(__FILE__) . '/ICSProjectService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSProject.php');
require_once(dirname(__FILE__) . '/../../models/CSProduct.php');

class CSProjectService extends CSServiceBase implements ICSProjectService
{
    public function getProjects($offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/project';

        $queryParams = array();
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $projectList = array();
        if (isset($responseArray['projects']))
        {
            foreach ($responseArray['projects'] as $projectData)
            {
                if ($projectData != null)
                {
                    array_push($projectList, new CSProject($projectData));
                }
            }
        }


        return $projectList;
    }

    public function getProject($projectId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/project/' . $projectId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['project']) ? new CSProject($responseArray['project']) : null;
    }

    public function createProject(/* CSProject */ $project, $masterUserName = null, $masterUserId = null)
    {
        $url = '/project';

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = ($project != null) ? $project->getRawData() : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['project']) ? new CSProject($responseArray['project']) : null;
    }

    public function getProductsForProject($projectId, $type = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/project/' . $projectId . '/products';

        $queryParams = array();
        if ($type != null)
        {
            $queryParams['type'] = $type;
        }
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $productList = array();
        if (isset($responseArray['products']))
        {
            foreach ($responseArray['products'] as $productData)
            {
                if ($productData != null)
                {
                    array_push($productList, new CSProduct($productData));
                }
            }
        }


        return $productList;
    }

    public function createProductForProject($projectId, $product, $mainImageURL = null, $subImageURLs = array(), $masterUserName = null, $masterUserId = null)
    {
        $url = '/project/' . $projectId . '/products';

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["productData"] = ($product != null) ? $product->getRawData() : array();
        $dataArray["mainImageURL"] = $mainImageURL;
        $dataArray["subImageURLs"] = ($subImageURLs != null && is_array($subImageURLs)) ? $subImageURLs : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['product']) ? new CSProduct($responseArray['product']) : null;
    }

    public function getProductVariationsForProject($projectId, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null)
    {
        $url = '/project/' . $projectId . '/variations';

        $queryParams = array();
        if ($offset >= 0)
        {
            $queryParams['offset'] = $offset;
        }
        if ($limit >= 0)
        {
            $queryParams['limit'] = $limit;
        }
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        $productList = array();
        if (isset($responseArray['products']))
        {
            foreach ($responseArray['products'] as $productData)
            {
                if ($productData != null)
                {
                    array_push($productList, new CSProduct($productData));
                }
            }
        }


        return $productList;
    }
}