<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 11:23 AM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSProjectService extends ICSService
{
    public function getProjects($offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
    public function getProject($projectId, $masterUserName = null, $masterUserId = null);
    public function createProject($project, $masterUserName = null, $masterUserId = null);
    public function getProductsForProject($projectId, $type = null, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
    public function createProductForProject($projectId, $product, $mainImageURL = null, $subImageURLs = array(), $masterUserName = null, $masterUserId = null);
    public function getProductVariationsForProject($projectId, $offset = -1, $limit = -1, $masterUserName = null, $masterUserId = null);
}