<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/24/14
 * Time: 9:51 PM
 */

require_once(dirname(__FILE__) . '/ICSQueueService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSPolling.php');

class CSQueueService extends CSServiceBase implements ICSQueueService
{
    public function getCommandStatus($token, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/queue/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        return isset($responseArray['status']) ? new CSPolling($responseArray['status']) : null;
    }
}