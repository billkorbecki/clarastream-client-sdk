<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/24/14
 * Time: 9:51 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSQueueService extends ICSService
{
    public function getCommandStatus($token, $masterUserName = null, $masterUserId = null);
}