<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 1/18/14
 * Time: 12:59 PM
 */

require_once(dirname(__FILE__) . '/ICSRuleEngineService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSPolling.php');
require_once(dirname(__FILE__) . '/../../models/CSSyncData.php');

final class CSRulesEngineService extends CSServiceBase implements ICSRulesEngineService
{
    public function createRulesEngineRequest($ruleSetId, $projectId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/rule/' . $ruleSetId . '/project/' . $projectId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = null;
        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray);

        //$tokenArray = $responseArray['tokenArray'];

        error_log("CSRulesEngineService Response = " . print_r($responseArray, 1));

        return $responseArray;
    }
} 