<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/ICSTemplateService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSTemplate.php');

class CSTemplateService extends CSServiceBase implements ICSTemplateService
{
    public function getTemplate($templateId, $masterUserName = null, $masterUserId = null)
    {
        $url = '/template/' . $templateId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['template']) ? new CSTemplate($responseArray['template']) : null;
    }

    public function createTemplate($template, $cartId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/template/cartId/' . $cartId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["templateData"] = ($template != null) ? $template->getRawData() : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "POST");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['template']) ? new CSProduct($responseArray['template']) : null;
    }

    public function updateTemplate($templateId, $template, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/template/' . $templateId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $dataArray = array();
        $dataArray["templateData"] = ($template != null) ? $template->getRawData() : array();

        $responseArray = $this->processCallToURL($url, $queryParams, $dataArray, "PUT");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['numAffected']) ? $responseArray['numAffected'] : -1;
    }

    // templateId is created within, and passed by reference back to the caller
    public function autoGenerateTemplateForCart($cartId, &$templateId, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/template/auto/cartId/' . $cartId;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams);

        // TODO (WK) Determine what to do with error situations...

        $token = $responseArray['token'];
        $templateId = $responseArray['templateId'];

        error_log("CSTemplateService::autoGenerateTemplateForCart: token = " . $token . "; templateId = " . $templateId);

        return $token;
    }

    public function getTemplateAutoGenerateStatus($token, $masterUserName = null, $masterUserId = null)
    {
        $url = parent::getBaseServiceURL() . '/template/auto/status/' . $token;

        $queryParams = array();
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        return isset($responseArray['status']) ? new CSPolling($responseArray['status']) : null;
    }
}