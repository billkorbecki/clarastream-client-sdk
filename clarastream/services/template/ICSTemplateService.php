<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 2/4/14
 * Time: 1:00 PM
 */

require_once(dirname(__FILE__) . '/../ICSService.php');

interface ICSTemplateService extends ICSService
{
    public function getTemplate($templateId, $masterUserName = null, $masterUserId = null);
    public function createTemplate($template, $cartId, $masterUserName = null, $masterUserId = null);
    public function updateTemplate($templateId, $template, $masterUserName = null, $masterUserId = null);
    public function autoGenerateTemplateForCart($cartId, &$templateId, $masterUserName = null, $masterUserId = null);
    public function getTemplateAutoGenerateStatus($token, $masterUserName = null, $masterUserId = null);
}