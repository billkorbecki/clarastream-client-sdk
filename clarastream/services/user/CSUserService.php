<?php
/**
 * Created by PhpStorm.
 * User: billkorbecki
 * Date: 3/19/14
 * Time: 1:19 AM
 */

require_once(dirname(__FILE__) . '/ICSUserService.php');
require_once(dirname(__FILE__) . '/../CSServiceBase.php');
require_once(dirname(__FILE__) . '/../../models/CSUser.php');

class CSUserService extends CSServiceBase implements ICSUserService
{
    public function getUserByUsername($username, $masterUserName = null, $masterUserId = null)
    {
        $url = '/user';

        $queryParams = array();
        $queryParams['username'] = "" . $username;
        if ($masterUserName != null)
        {
            $queryParams['masterUserName'] = "" . $masterUserName;
        }
        if ($masterUserId != null)
        {
            $queryParams['masterUserId'] = "" . $masterUserId;
        }

        $responseArray = $this->processCallToURL($url, $queryParams, null, "GET");

        // TODO (WK) Determine what to do with error situations...

        return isset($responseArray['user']) ? new CSUser($responseArray['user']) : null;
    }
}